#include <math.h>

int solution(int A, int B) {
	int sqrtA = sqrt(A);
	int sqrtB = sqrt(B);
		
	return (sqrtB - sqrtA + 1);
}