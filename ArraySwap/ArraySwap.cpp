#include <vector>

using namespace std;

bool isSorted(vector<int> &A) {
	int len = A.size();
	bool ret = true;
	for (i = 0; i < len; ++i) {
        if (A[i] > A[i+1]) {
			ret = false;
            break;
		}
    }

	return ret;
}

int solution(vector<int> &A) {
	int len = A.size();
    if (len == 0)
        return 0;
    if (len == 1 || len == 2)
        return 1;
    
    if (isSorted(A))
		return 1;
    
	int i = 0;
    int peak = 0;
	for (i = 1; i < len; ++i) {
        if (A[i] < A[i - 1]) {
            peak = i - 1;
			break;
		}
    }
    
    i = peak + 1;
    while(i < len) {
		if (A[i] > A[peak]) {
			swap(A[peak], A[i-1]);
			break;
		}
		if (i == len - 1) 
			swap(A[peak], A[i]);
		++i;
    }

    if (isSorted(A))
		return 1;
    
    return 0;
}