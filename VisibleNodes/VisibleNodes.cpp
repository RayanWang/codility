int compute(tree* node, int currMax) {
	if (!node)
		return 0;

	if (node->x > currMax)
		return (1+compute(node->left, node->x)+compute(node->right, node->x));
	else
		return (compute(node->left, currMax)+compute(node->right, currMax));
}

int solution(tree* T) {
	int max = INT_MIN;
		
	return compute(T, max);
}